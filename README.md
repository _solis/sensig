# IR Falling Classification System

Final Winter Semester 2018/19 project for Sensor Signal Processing taught by Prof. Dr.-Ing. Andreas König under Lehrstuhl Integrierte Sensorsysteme at Tchnische Universität Kaiserslautern

The main objective of this project is to identify images as either "fall" or "standing".  The motivation for this project is to classify when an elderly person has fallen. For this case, a Raspberry Pi 3 Model B Rev 1.2 with a Raspberry Pi IR Camera Module was used to take the photos.

## Direcotry Structure
```
├── images                  # Contains all images to be classified
│   ├ for_forground         #   Foreground and background images
│   ├ test_set              #   Images used for test set
│   ├ training_set          #   Images used for training set
├── presentation            # Final presentation as editable .odb and and final .pdf
├── results                 # Previous results recorded
│   ├ fail_imgs             #   Images that previous results failed to classify
│   ├ step_imgs             #   Step-by-step image output of processing
├── scripts                 # Bash script to install OpenCV and fallClassifier.py
└── README.md
```
## Requirements
Directory and file structure is crucial for scripts to run. Keep those intact.

### Images
It is assumed images are located in images/ and that their titles are {ground_truth}.{image_number}.jpg

IR Camera set for 640 x 480 resolution, one background image taken.
- Training set – 50 pictures of me standing, 50 pictures of me “falling”
- Test set – 25 “falling”, 23 standing, 2 sitting
- All taken with the Raspberry Pi using a simple Python interface to the Camera Module

### OpenCV
It is assumed OpenCV 3.0+ is installed in the running computer. If this is not the case, a script called "installOpenCV.sh" has been written for this purpose.  This is an absolute must as OpenCV APIs are used at every step of image processing.

## Usage and Code Description

"fallClassifier.py" code is a heavily modified version from article [k-NN classifier for image classification](https://www.pyimagesearch.com/2016/08/08/k-nn-classifier-for-image-classification/)

The code uses the processed image's pixels as the feature vector for both training and testing. Test is not done with by splitting training and test data, individual data was take for each purpose. It uses a KNN classifer for prediciton. After training and testing, the script calculates the accuracy by using the image's title (ground truth label) and the prediction. It also measures latencies for image processing, model training, and model testing. **Note: KNN classifer is dependent on the sklearn package **

To run: simply execute fallClassifier.py

## Results Output Example
```
[RESULTS]
 Preprocessing Time: 2443.72ms
 Training Time:      2.15ms
 Test Time:          6.98ms
 ACCURACY:           70.00%
```


