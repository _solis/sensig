#!/bin/bash
Black='\033[0;30m'
DarkGray='\033[1;30m'
RED='\033[0;31m'
LightRed='\033[1;31m'
Green='\033[0;32m'
LightGreen='\033[1;32m'
BrownOrange='\033[0;33m'
Yellow='\033[1;33m'
Blue='\033[0;34m'
LightBlue='\033[1;34m'
Purple='\033[0;35m'
LightPurple='\033[1;35m'
Cyan='\033[0;36m'
LightCyan='\033[1;36m'
LightGray='\033[0;37m'
White='\033[1;37m'
NC='\033[0m'

MODEL="$(cat /proc/cpuinfo | grep 'model name')"
if [[ "$MODEL" =~ "ARM" ]]; 
then
    echo -e "${LightGreen}Running from Raspberry Pi"
    echo "Execute command in OTHER terminal:"
    echo "   sudo raspi-config"
    echo "EXPAND FILESYSTEM:"
    echo "   7 Advanced Options -> A1 Expand Filesystem -> finish"
    echo "Execute command:"
    echo -e "   sudo reboot${LightRed}"
    read -n 1 -s -r -p "Did you do it? Press any key to continue..."
    echo -e "${NC}"
else 
    echo -e "${Yellow}Running from $MODEL${NC}"
fi

echo -e "${LightGreen}\nrunning update && upgrade...${NC}"
sudo apt-get update
sudo apt-get upgrade

echo -e "${LightGreen}installing libraries for build...${NC}"
sudo apt-get install build-essential cmake pkg-config unzip

# Install image and video libraries
echo -e "${LightGreen}installing image and video libraries...${NC}"
sudo apt-get install libjpeg-dev libpng-dev libtiff-dev
sudo apt-get install libavcodec-dev libavformat-dev libswscale-dev libv4l-dev
sudo apt-get install libxvidcore-dev libx264-dev

# Install GTK for GUI backend
echo -e "${LightGreen}installing GTK for GUI backend...${NC}"
sudo apt-get install libgtk-3-dev
sudo apt-get install libcanberra-gtk*

# Install mathimatical optimizations for OpenCV
echo -e "${LightGreen}installing mathimatical optimizations for OpenCV...${NC}"
sudo apt-get install libatlas-base-dev gfortran

# Install Python3 dev headers
echo -e "${LightGreen}installing Python3 dev headers...${NC}"
sudo apt-get install python3-dev

# Download OpenCV source
echo -e "${LightGreen}downloading OpenCV source...${NC}"
cd ~
wget -O opencv.zip https://github.com/opencv/opencv/archive/4.0.1.zip
wget -O opencv_contrib.zip https://github.com/opencv/opencv_contrib/archive/4.0.1.zip
unzip opencv.zip && mv opencv-4.0.1/ opencv
unzip opencv_contrib.zip && mv opencv_contrib-4.0.1/ opencv_contrib

# Configure Python environment
echo -e "${LightGreen}configuring Python3 environment...${NC}"
wget https://bootstrap.pypa.io/get-pip.py
sudo python3 get-pip.py
sudo pip install numpy

# Build OpenCV
echo -e "${LightGreen}Build OpenCV...${NC}"
cd ~/opencv
mkdir build
cd build
FLAGS="-D CMAKE_BUILD_TYPE=RELEASE       \
	    -D CMAKE_INSTALL_PREFIX=/usr/local  \
	    -D INSTALL_PYTHON_EXAMPLES=OFF      \
	    -D BUILD_opencv_python2=OFF         \
	    -D BUILD_opencv_python3=ON          \
	    -D INSTALL_C_EXAMPLES=OFF           \
	    -D OPENCV_ENABLE_NONFREE=ON         \
	    -D OPENCV_EXTRA_MODULES_PATH=~/opencv_contrib/modules   \
	    -D PYTHON_EXECUTABLE=/usr/bin/python3                  \
	    -D BUILD_EXAMPLES=OFF .."
if [[ "$MODEL" =~ "ARM" ]]; then
    FLAGS="-D CMAKE_BUILD_TYPE=RELEASE       \
	    -D CMAKE_INSTALL_PREFIX=/usr/local  \
	    -D INSTALL_PYTHON_EXAMPLES=OFF      \
	    -D BUILD_opencv_python2=OFF         \
	    -D BUILD_opencv_python3=ON          \
	    -D ENABLE_NEON=ON                   \
	    -D ENABLE_VFPV3=ON                  \
	    -D INSTALL_C_EXAMPLES=OFF           \
	    -D OPENCV_ENABLE_NONFREE=ON         \
	    -D OPENCV_EXTRA_MODULES_PATH=~/opencv_contrib/modules   \
	    -D PYTHON_EXECUTABLE=/usr/local/bin/python3.7           \
	    -D BUILD_EXAMPLES=OFF .."
fi
echo "cmake $FLAGS"
cmake $FLAGS
if [[ "$MODEL" =~ "ARM" ]]; then
    echo -e "${Yellow}WARNING: Please increase RPi's SWAPSIZE to stop memory hangs during compile"
    echo "Execute command in OTHER terminal:"
    echo "   sudo vi(vim/gedit/emacs) /etc/dphys-swapfile"
    echo "Change:"
    echo "   CONF_SWAPSIZE=100 to 1024"
    echo "Execute commands:"
    echo "   sudo /etc/init.d/dphys-swapfile stop"
    echo -e "   sudo /etc/init.d/dphys-swapfile start ${LightRed}"
    read -n 1 -s -r -p "Did you do it? Press any key to start build..."
    echo -e "${NC}"
fi

# Get core number so we can compile faster
CORES="$(cat /proc/cpuinfo | grep 'processor' | wc -l)"
echo -e "${LightGreen}compiling on $CORES cores...${NC}"
sudo make -j $CORES
sudo make install
sudo ldconfig

# Run example to very OpenCV installation
echo -e "${LightRed}Verify OpenCV 4.0.1:"
echo "Execute Command:"
echo "   python3"
echo "RUN:"
echo "   import cv2"
echo "   cv2.__version__"
echo "   quit()"

# Clean up
if [[ "$MODEL" =~ "ARM" ]]; then
    echo -e "${Yellow}WARNING: Change back SWAPSIZE"
    echo "Execute command in OTHER terminal:"
    echo "   vi /etc/dphys-swapfile"
    echo "Change:"
    echo "   CONF_SWAPSIZE=1024 to 100"
    echo "Execute commands:"
    echo "   sudo /etc/init.d/dphys-swapfile stop"
    echo "   sudo /etc/init.d/dphys-swapfile start${LightRed}"
    read -n 1 -s -r -p "Did you do it? Press any key to finish..."
    echo -e "${NC}"
fi
cd ~
sudo rm -rf get-pip.py opencv/ opencv_contrib opencv.zip opencv_contrib.zip 
echo -e "${LightCyan}...all done!${NC}"
