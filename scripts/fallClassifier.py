#!/usr/bin/python3
import time
import cv2
import numpy as np
from imutils import paths
import os
from sklearn.neighbors import KNeighborsClassifier
from sklearn.model_selection import train_test_split
#####################################################################
# Utility Funtions
#####################################################################
# Converts an image to a feature vector
def imageToFeatureVector(image, size=(32,32)):
   # Resize the image and flatten to raw pixel intensities
   return cv2.resize(image, size).flatten()
# Preprocessor
def preProcessor(image, bgImg, fbg):
   grayImg  = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
   fgmask   = fbg.apply(bgImg)
   # Remove background
   fgmask   = fbg.apply(grayImg)
   # Gaussian filter to filter noise
   blur = cv2.GaussianBlur(fgmask, (13,13), 0)
   # Threshold to remove noise
   _,th = cv2.threshold(blur, 0, 255, cv2.THRESH_BINARY+cv2.THRESH_OTSU)

   #cv2.imshow("bgImg", bgImg)   
   #cv2.imshow("grayImg", grayImg)
   #cv2.imshow("fgmask", fgmask)
   #cv2.imshow("blur", blur)
   #cv2.imshow("th", th)
   #while True:
   #   key = cv2.waitKey(1) & 0xFF
   #   if key == ord("q"):
   #      cv2.destroyAllWindows()
   #      break
   return th
#####################################################################
# Read Images and Preprocess Training Data
#####################################################################
print("[INFO] Prepocessing Training Set...")
trainingImgs   = list(paths.list_images("../images/training_set"))
# feature matrix and labels list
features_training    = []
labels_training      = []
# Extract image and class label for TRAINING data, assumes image title as {label}.{imgNum}.jpg
processTimeStart = time.time()
# This is used so that we can subtract the background from all upcoming images
background  = cv2.imread("../images/for_foreground/background.jpg")
grayBg      = cv2.cvtColor(background, cv2.COLOR_BGR2GRAY)
bgSub       = cv2.createBackgroundSubtractorMOG2()
for(i, trainingImg) in enumerate(trainingImgs):
   image          = cv2.imread(trainingImg)
   label          = trainingImg.split(os.path.sep)[-1].split(".")[0]
   # Process the image
   processedImg   = preProcessor(image, grayBg, bgSub)
   # Extract feautres (pixels) of data
   pixels         = imageToFeatureVector(processedImg)
   # Append relavent data
   features_training.append(pixels)
   labels_training.append(label)
   # Uncomment to display processed images
   #imgTitle = "Processed image {}".format(label)
   #cv2.imshow(imgTitle, processedImg)
   #while True:
   #   key = cv2.waitKey(1) & 0xFF
   #   if key == ord("q"):
   #      cv2.destroyAllWindows()
   #      break
   # Show update very 5 pictures
   #if i > 0 and i % 5 == 0:
   #   print("[INFO] processed {}/{} training images".format(i, len(trainingImgs)))
# Convert to Numpy arrays
features_training    = np.array(features_training)
labels_training      = np.array(labels_training)
processTimeEnd       = time.time()
processTime          = processTimeEnd - processTimeStart
#####################################################################
# Process Test Data
#####################################################################
print("[INFO] Prepocessing Test Set...")
# Extract image and features for TEST. It is assumed image is titled {truth}.{imgNum}.jpg
testImgs       = list(paths.list_images("../images/test_set"))
features_test  = []
real_truth     = []
processTimeStart = time.time()
for(i, testImg) in enumerate(testImgs):
   image          = cv2.imread(testImg)
   truth          = testImg.split(os.path.sep)[-1].split(".")[0]
   # Process the image
   processedImg   = preProcessor(image, grayBg, bgSub)
   # Extract features (pixels)
   pixels         = imageToFeatureVector(processedImg)
   # Append relavent data
   features_test.append(pixels)
   real_truth.append(truth)
   # Uncomment to display processed images
   #imgTitle = "Processed image {}".format(truth)
   #cv2.imshow(imgTitle, processedImg)
   #while True:
   #   key = cv2.waitKey(1) & 0xFF
   #   if key == ord("q"):
   #      cv2.destroyAllWindows()
   #      break
   # Show update very 5 pictures
   #if i > 0 and i % 5 == 0:
   #   print("[INFO] processed {}/{} test images".format(i, len(trainingImgs)))
# Covert to Numpy arrays
features_test  = np.array(features_test)
real_truth     = np.array(real_truth)
processTimeEnd = time.time()
processTime    += processTime + (processTimeEnd - processTimeStart)
#####################################################################
# Training
#####################################################################
print("[INFO] Training KNN Model...")
# 5 nearest neighbors using Manhattan distance
model             = KNeighborsClassifier(n_neighbors=5, p=1)
trainingTimeStart = time.time()
model.fit(features_training, labels_training)
trainingTimeEnd   = time.time()
trainingTime      = trainingTimeEnd - trainingTimeStart
correct           = 0
fails             = 0
#####################################################################
# Testing
#####################################################################
# Predict on the features acquired from test images and save them to array
print("[INFO] Predicting Results...")
testTimeStart  = time.time()
predictions    = model.predict(features_test)
testTimeEnd    = time.time()
testTime       = testTimeEnd - testTimeStart

# Compare the real truth (labels in test images) with the prediciton
# There were two pictures that were labeled as "sitting", those are not
# falls, so they should be predicted as "standing"
for i in range(predictions.shape[0]):
   if np.char.equal(real_truth[i], "sitting"):
      if np.char.not_equal(predictions[i], "standing"):
         fails += 1
      else:
         correct += 1
      continue
   if np.char.equal(real_truth[i], predictions[i]):
      correct += 1
   else:
      fails += 1
      
acc = correct / len(predictions)
outputFile = open("../results/results.txt", "w")
outputFile.write("[RESULTS]\n")
outputFile.write(" Preprocessing Time: {:.2f}ms\n".format(processTime*1000))
outputFile.write(" Training Time:      {:.2f}ms\n".format(trainingTime*1000))
outputFile.write(" Test Time:          {:.2f}ms\n".format(testTime*1000))
outputFile.write(" ACCURACY:           {:.2f}%\n".format(acc*100))

print("[RESULTS]")
print(" Preprocessing Time: {:.2f}ms".format(processTime*1000))
print(" Training Time:      {:.2f}ms".format(trainingTime*1000))
print(" Test Time:          {:.2f}ms".format(testTime*1000))
print(" ACCURACY:           {:.2f}%".format(acc*100))
outputFile.close()

